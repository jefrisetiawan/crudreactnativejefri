import { Component, useState } from "react";
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Button,
} from "react-native";

// function dimulai dengan huruf besar => Class
function App() {
  // let numb = 1
  // state
  // addNumber = menampilkan data yang useState
  // setNumber = memberikan value ke State
  const [addNumber, setNumber] = useState(0);
  const [addString, setString] = useState("");

  const [fromChild, setFromChild] = useState("");

  const penjumlahan = (nilai) => {
    if (nilai === "plus") {
      setNumber(addNumber + 1);
      setString("Plus");
    }
    if (nilai === "minus") {
      setNumber(addNumber - 1);
      setString("Minus");
    }
  };

  return (
    <View style={styles.container}>
      <Text style={{ marginTop: 30, fontSize: 50 }}>
        Hello World! {addNumber} {fromChild}
      </Text>
      <TouchableOpacity
        onPress={() => penjumlahan("plus")}
        style={{ backgroundColor: "grey" }}
      >
        <Text style={{ fontSize: 50, color: "white" }}>Ini Button +</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => penjumlahan("minus")}
        style={{ backgroundColor: "white" }}
      >
        <Text style={{ fontSize: 50 }}>Ini Button -</Text>
      </TouchableOpacity>
      <AddImage />

      {/* props */}
      <InputText
        propNumber={addNumber}
        propString={addString}
        sendProps={(val) => setFromChild(val)}
      />
      <TextClass />
    </View>
  );
}

const AddImage = () => {
  return (
    <Image
      style={{ width: 100, height: 100 }}
      source={{ uri: "https://placeimg.com/200/200/tech" }}
    />
  );
};

const InputText = ({ propNumber, propString, sendProps }) => {
  return (
    <View>
      <TextInput style={{ borderWidth: 1 }} />
      <Text style={{ borderWidth: 1, fontSize: 30 }}>
        Text {propNumber} {propString}
      </Text>
      <Button
        onPress={() => sendProps("Ini dari Child")}
        title="Send to Parent"
      />
    </View>
  );
};

class TextClass extends Component {
  render() {
    return (
      <View
        style={{
          marginTop: 20,
          borderWidth: 1,
          flex: 1,
          flexDirection: "row",
          // justifyContent: 'center',
          // alignItems: "center"
        }}
      >
        <View
          style={{
            borderWidth: 1,
            backgroundColor: "salmon",
            width: 100,
            height: 100,
            alignSelf: "center",
          }}
        />

        <View
          style={{
            borderWidth: 1,
            backgroundColor: "cyan",
            width: 100,
            height: 100,
            alignSelf: "flex-end",
          }}
        />
        {/* <View style={{borderWidth: 1, backgroundColor: "green", flex: 1}}/>
        <View style={{borderWidth: 1, backgroundColor: "blue", flex: 1}}/> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ddd",
  },
});

export default App;
