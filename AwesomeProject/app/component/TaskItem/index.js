import { View, StyleSheet, Image } from "react-native";
import Text from "../Text";
import React from "react";
import images from "../../assets/images";

const TaskItem = (props) => {
  const stat = props.status;
  return (
    <View style={{ marginTop: 18 }}>
      <View
        style={{
          borderWidth: 1,
          borderColor: "#A7A7A7",
          borderRadius: 12,
          backgroundColor: "#FFFFFF",
          padding: 16,
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image
            source={images.iTASK}
            style={{ width: 28, height: 28, marginRight: 10 }}
          />
          <View style={{ flex: 1 }}>
            <Text fontSize={18} color="#160520" bold>
              {props.title}
            </Text>
          </View>
        </View>

        <View style={{ flexDirection: "row", marginTop: 18 }}>
          <Image
            source={images.iFLAG}
            style={{ width: 12, height: 16.18, marginRight: 6 }}
          />
          <Text fontSize={11} color="#7A7A7A">
            {props.date}
          </Text>
        </View>

        <Text fontSize={11} style={{ marginTop: 11 }}>
          {props.activities}
        </Text>

        <View style={{ marginTop: 7, flexDirection: "row" }}>
          <View
            style={{
              backgroundColor:
                props.status == "Present"
                  ? "#70A1FF"
                  : props.status == "Leave"
                  ? "#FDE180"
                  : props.status == "Absent"
                  ? "#EF9F9F"
                  : "#ECCC68",
              borderRadius: 8,
              paddingVertical: 2,
              paddingHorizontal: 10,
              alignSelf: "flex-start",
              marginRight: 11,
            }}
          >
            <Text fontSize={10} bold color="white">
              {props.status}
            </Text>
          </View>

          <View
            style={{
              backgroundColor: "#CED1D4",
              borderRadius: 8,
              paddingVertical: 2,
              paddingHorizontal: 10,
              alignSelf: "flex-start",
            }}
          >
            <Text fontSize={10} bold color="white">
              {props.start} - {props.end}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default TaskItem;

const styles = StyleSheet.create({});
