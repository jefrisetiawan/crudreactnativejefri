export default {
  "Nunito-Regular": require("./Nunito-Regular.ttf"),
  "Nunito-Bold": require("./Nunito-Bold.ttf"),
  "Nunito-BoldItalic": require("./Nunito-BoldItalic.ttf"),
  "Nunito-SemiBold": require("./Nunito-SemiBold.ttf"),
  "Nunito-SemiBoldItalic": require("./Nunito-SemiBoldItalic.ttf"),
};
