import HomeIcon from "./svg/home.svg";
import ProfileIcon from "./svg/profile.svg";
import TaskIcon from "./svg/task.svg";
import PerformIcon from "./svg/perform.svg";
import EyeIcon from "./svg/eye.svg";
import EyeSlashIcon from "./svg/eyeSlash.svg";
import ArrowIcon from "./svg/arrow.svg";
import DateIcon from "./svg/date.svg";
import ArrowDownIcon from "./svg/arrowDown.svg";

export default {
  BACKGROUND: require("./bgProfile.png"),
  PERSON1: require("./person2.png"),
  PERSON2: require("./person6.png"),
  PERSON3: require("./person10.png"),
  PERSON4: require("./person3.png"),
  rARROW: require("./Vector.png"),
  iGUARD: require("./guard.png"),
  iHELP: require("./help.png"),
  iABOUT: require("./about.png"),
  iLOGOUT: require("./logout.png"),
  vGUARD: require("./guardV.svg"),
  ADD: require("./add.png"),
  BGSCREEN: require("./bgScreen.png"),
  iTASK: require("./task.png"),
  iFLAG: require("./flag.png"),
};

export {
  HomeIcon,
  ProfileIcon,
  TaskIcon,
  PerformIcon,
  EyeIcon,
  EyeSlashIcon,
  ArrowIcon,
  DateIcon,
  ArrowDownIcon,
};
