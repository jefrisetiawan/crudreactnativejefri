import { ScrollView, TextInput, View } from "react-native";
import React, { useState } from "react";
import Text from "../../component/Text";
import { Fonts, HEIGHT, WIDTH } from "../../assets/styles";
import { ArrowDownIcon, ArrowIcon, DateIcon } from "../../assets/images";
import { TouchableOpacity } from "react-native";
import { Picker } from "@react-native-picker/picker";
import { DateTimePickerAndroid } from "@react-native-community/datetimepicker";
// import { addDoc, collection } from "firebase/firestore";
import {
  app,
  db,
  getFirestore,
  collection,
  addDoc,
} from "../../../firebaseConfig.js";

const Add = ({ navigation, route }) => {
  const dataStatus = ["Select", "Present", "Leave", "Absent", "Sick"];

  // DatePicker
  const [date, setDate] = useState(new Date());
  const [timeStart, setTimeStart] = useState("Kosong");
  const [timeEnd, setTimeEnd] = useState("Kosong");
  const [tanggal, setTanggal] = useState("Select Date");
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);

    let tempDate = new Date(currentDate);
    let fDate =
      monthNames[tempDate.getMonth()] +
      " " +
      tempDate.getDate() +
      ", " +
      tempDate.getFullYear();
    let fTimeS = tempDate.getHours() + ":" + tempDate.getMinutes();
    setTanggal(fDate);
    setTimeStart(fTimeS);
    setTimeEnd(fTimeS);
  };

  const showMode = (currentMode) => {
    DateTimePickerAndroid.open({
      value: date,
      onChange,
      mode: currentMode,
      is24Hour: true,
    });
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };

  // Value

  const [title, setTitle] = useState("");
  // const [start, setStart] = useState("");
  // const [end, setEnd] = useState("");
  const [selectedStatus, setSelectedStatus] = useState(0);
  const [activities, setActivities] = useState("");

  const addTask = async () => {
    try {
      const doc = await addDoc(collection(db, "tasks"), {
        title: title,
        date: tanggal,
        start: timeStart,
        end: timeEnd,
        status: selectedStatus,
        activities: activities,
      });
      console.log("doc written with ID: ", doc.id);
      navigation.navigate("Task");
      setTitle("");
      setTanggal("");
      setTimeStart("");
      setTimeEnd("");
      setSelectedStatus(0);
      setActivities("");
    } catch (e) {
      console.error("error addoc doc", e);
    }
  };

  return (
    <ScrollView>
      <View>
        <View style={{ backgroundColor: "white", width: WIDTH, height: 870 }}>
          <View style={{ marginTop: 54, marginHorizontal: 16 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <TouchableOpacity onPress={() => navigation.navigate("Task")}>
                <ArrowIcon width={20} height={14.28} stroke={"#160520"} />
              </TouchableOpacity>
              <Text
                style={{ marginLeft: 53 }}
                fontSize={24}
                bold
                color="#160520"
              >
                Time Sheet Form
              </Text>
            </View>
          </View>
          {/* Form */}
          <View style={{ marginTop: 25, marginHorizontal: 16 }}>
            {/* Title */}
            <View>
              <Text fontSize={16} bold color="#160520">
                Title
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderWidth: 1,
                  borderColor: "#A7A7A7",
                  borderRadius: 8,
                  backgroundColor: "#FBFBFB",
                  padding: 12,
                }}
              >
                <TextInput
                  style={{
                    // color: "#D3D3D3",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Task Title"
                  value={title}
                  onChangeText={(txt) => {
                    console.log("VALUE TITLE", txt);
                    setTitle(txt);
                  }}
                ></TextInput>
              </View>
            </View>

            {/* Date */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} bold color="#160520">
                Date
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderWidth: 1,
                  borderColor: "#A7A7A7",
                  borderRadius: 8,
                  backgroundColor: "#FBFBFB",
                  padding: 12,
                }}
              >
                <TextInput
                  style={{
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder={tanggal}
                  value={date}
                ></TextInput>

                <TouchableOpacity
                  onPress={showDatepicker}
                  style={{ position: "absolute", right: 12, top: 15 }}
                >
                  <DateIcon width={20} height={22.22} />
                </TouchableOpacity>
              </View>
            </View>

            {/* Time */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} bold color="#160520">
                Time
              </Text>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={{
                    flex: 1,
                    marginTop: 8,
                    borderWidth: 1,
                    borderColor: "#A7A7A7",
                    borderRadius: 8,
                    backgroundColor: "#FBFBFB",
                    padding: 12,
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity onPress={showTimepicker}>
                      <TextInput
                        editable={false}
                        style={{
                          fontSize: 16,
                          fontFamily: Fonts.Nunito.Regular,
                        }}
                        placeholder={timeStart}
                      ></TextInput>
                    </TouchableOpacity>
                  </View>
                </View>

                <View
                  style={{
                    alignSelf: "center",
                    marginHorizontal: 12,
                    width: 18,
                    borderTopWidth: 3,
                    borderColor: "#160520",
                  }}
                ></View>

                <View
                  style={{
                    flex: 1,
                    marginTop: 8,
                    borderWidth: 1,
                    borderColor: "#A7A7A7",
                    borderRadius: 8,
                    backgroundColor: "#FBFBFB",
                    padding: 12,
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity onPress={showTimepicker}>
                      <TextInput
                        editable={false}
                        style={{
                          fontSize: 16,
                          fontFamily: Fonts.Nunito.Regular,
                        }}
                        placeholder={timeStart}
                      ></TextInput>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>

            {/* Status */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} bold color="#160520">
                Status
              </Text>

              <View
                style={{
                  marginTop: 8,
                  borderWidth: 1,
                  borderColor: "#A7A7A7",
                  borderRadius: 8,
                  backgroundColor: "#FBFBFB",

                  // padding: 12,
                }}
              >
                <Picker
                  selectedValue={selectedStatus}
                  onValueChange={(itemValue, index) => {
                    setSelectedStatus(itemValue);
                    console.log(itemValue);
                  }}
                  mode="dialog"
                >
                  {dataStatus.map((status, index) => (
                    <Picker.Item label={status} value={status} key={index} />
                  ))}
                  {/* <Picker.Item value="" label="Placeholder text..." />
                  <Picker.Item label="Present" value="Present" />
                  <Picker.Item label="Leave" value="Leave" />
                  <Picker.Item label="Absent" value="Absent" />
                  <Picker.Item label="Sick" value="Sick" /> */}
                </Picker>
              </View>
            </View>

            {/* Activities */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} bold color="#160520">
                Activities
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderWidth: 1,
                  borderColor: "#A7A7A7",
                  borderRadius: 8,
                  backgroundColor: "#FBFBFB",
                  paddingHorizontal: 12,
                  paddingVertical: 12,
                }}
              >
                <TextInput
                  multiline={true}
                  numberOfLines={7}
                  style={{
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                    height: 134,
                    textAlignVertical: "top",
                  }}
                  placeholder="Write Your Activities"
                  value={activities}
                  onChangeText={(txt) => {
                    console.log("VALUE ACTIVITIES", txt);
                    setActivities(txt);
                  }}
                ></TextInput>
              </View>
            </View>
          </View>
          {/* Button */}
          <TouchableOpacity
            onPress={() => addTask()}
            style={{
              marginHorizontal: 16,
              backgroundColor: "#18DCFF",
              borderRadius: 8,
              paddingVertical: 12,
              alignItems: "center",
              marginTop: 50,
              marginBottom: 24,
            }}
          >
            <Text>Register</Text>
          </TouchableOpacity>
          {/* End Button */}
        </View>
      </View>
    </ScrollView>
  );
};

export default Add;
