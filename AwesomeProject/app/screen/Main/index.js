import { View, Text, Button, Touchable, Image } from "react-native";
import React from "react";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Profile from "../Profile";
import Home from "../Home";
import Task from "../Task";
import Performance from "../Perform";
import Add from "../Add";
import {
  HomeIcon,
  ProfileIcon,
  TaskIcon,
  PerformIcon,
} from "../../assets/images";
import images from "../../assets/images";
import { TouchableOpacity } from "react-native-gesture-handler";

const Tab = createBottomTabNavigator();

function Main({ navigation, route }) {
  return (
    <View style={{ flex: 1 }}>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarActiveTintColor: "#04325F",
          tabBarInactiveTintColor: "#CED1D4",

          tabBarIcon: ({ focused, color, size }) => {
            const tabBarIcon = {
              Home: <HomeIcon width={size} height={size} fill={color} />,
              Profile: <ProfileIcon width={size} height={size} fill={color} />,
              Add: (
                <TouchableOpacity>
                  <Image
                    source={images.ADD}
                    style={{ width: 110, height: 110, marginBottom: 40 }}
                  />
                </TouchableOpacity>
              ),
              Task: <TaskIcon width={size} height={size} fill={color} />,
              Performance: (
                <PerformIcon width={size} height={size} fill={color} />
              ),
            };
            return <View>{tabBarIcon[route.name]}</View>;
          },
          tabBarStyle: {
            paddingBottom: 10,
            paddingTop: 10,
            height: 70,
          },
        })}
      >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Task" component={Task} />
        <Tab.Screen
          name="Add"
          component={Add}
          options={() => ({
            tabBarLabelStyle: { display: "none" },
            tabBarStyle: {
              display: "none",
            },
          })}
        />
        <Tab.Screen name="Performance" component={Performance} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </View>
  );
}

export default Main;
