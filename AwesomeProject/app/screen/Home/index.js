import { View, Text } from "react-native";
import React from "react";
import { HomeIcon } from "../../assets/images";

const Home = () => {
  return (
    <View>
      <Text
        style={{
          fontSize: 30,
          textAlign: "center",
          marginTop: 250,
        }}
      >
        Home
      </Text>
    </View>
  );
};

export default Home;
