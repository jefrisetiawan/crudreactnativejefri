import {
  View,
  StyleSheet,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Text from "../../component/Text";
import React, { useState } from "react";
import images, { ArrowIcon, EyeIcon, EyeSlashIcon } from "../../assets/images";
import { HEIGHT, WIDTH } from "../../assets/styles";
import { Fonts } from "../../assets/styles";

const Register = ({ navigation, route }) => {
  const [visible, setVisible] = useState(true);
  const [visible2, setVisible2] = useState(true);

  return (
    <ScrollView>
      <View>
        <ImageBackground
          source={images.BGSCREEN}
          resizeMode="cover"
          style={{ width: WIDTH, height: 900 }}
        >
          <View style={{ marginTop: 55, marginHorizontal: 16 }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <ArrowIcon width={20} height={14.28} stroke={"white"} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 54, marginHorizontal: 16 }}>
            {/* Name */}
            <View>
              <Text fontSize={16} color="#FFF">
                Name
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderColor: "#132040",
                  borderRadius: 8,
                  borderWidth: 1,
                  padding: 12,
                  backgroundColor: "#273C75",
                }}
              >
                <TextInput
                  style={{
                    color: "#FFF",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Your Name"
                  placeholderTextColor={"#D3D3D3"}
                />
              </View>
            </View>
            {/* End Name */}

            {/* Email */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} color="#FFF">
                Email
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderColor: "#132040",
                  borderRadius: 8,
                  borderWidth: 1,
                  padding: 12,
                  backgroundColor: "#273C75",
                }}
              >
                <TextInput
                  keyboardType="email-address"
                  inputMode="email"
                  style={{
                    color: "#FFF",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Your Email"
                  placeholderTextColor={"#D3D3D3"}
                />
              </View>
            </View>
            {/* End Email */}

            {/* Phone */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} color="#FFF">
                Phone
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderColor: "#132040",
                  borderRadius: 8,
                  borderWidth: 1,
                  padding: 12,
                  backgroundColor: "#273C75",
                }}
              >
                <TextInput
                  keyboardType="phone-pad"
                  style={{
                    color: "#FFF",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Your Phone"
                  placeholderTextColor={"#D3D3D3"}
                />
              </View>
            </View>
            {/* End Phone */}

            {/* NIK */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} color="#FFF">
                NIK
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderColor: "#132040",
                  borderRadius: 8,
                  borderWidth: 1,
                  padding: 12,
                  backgroundColor: "#273C75",
                }}
              >
                <TextInput
                  keyboardType="number-pad"
                  style={{
                    color: "#FFF",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Your NIK"
                  placeholderTextColor={"#D3D3D3"}
                />
              </View>
            </View>
            {/* End NIK */}

            {/* Password */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} color="#FFF">
                Password
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderColor: "#132040",
                  borderRadius: 8,
                  borderWidth: 1,
                  padding: 12,
                  backgroundColor: "#273C75",
                }}
              >
                <TextInput
                  secureTextEntry={visible}
                  style={{
                    color: "#FFF",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Your Password"
                  placeholderTextColor={"#D3D3D3"}
                />

                <TouchableOpacity
                  onPress={() => setVisible(!visible)}
                  style={{
                    position: "absolute",
                    right: 12,
                    top: 15,
                  }}
                >
                  {visible ? (
                    <EyeIcon width={20} height={20} />
                  ) : (
                    <EyeSlashIcon width={20} height={20} />
                  )}
                </TouchableOpacity>
              </View>
            </View>
            {/* End Password */}

            {/* Confirm Password */}
            <View style={{ marginTop: 27 }}>
              <Text fontSize={16} color="#FFF">
                Confirm Password
              </Text>
              <View
                style={{
                  marginTop: 8,
                  borderColor: "#132040",
                  borderRadius: 8,
                  borderWidth: 1,
                  padding: 12,
                  backgroundColor: "#273C75",
                }}
              >
                <TextInput
                  secureTextEntry={visible2}
                  style={{
                    color: "#FFF",
                    fontSize: 16,
                    fontFamily: Fonts.Nunito.Regular,
                  }}
                  placeholder="Enter Your Confirm Password"
                  placeholderTextColor={"#D3D3D3"}
                />

                <TouchableOpacity
                  onPress={() => setVisible2(!visible2)}
                  style={{
                    position: "absolute",
                    right: 12,
                    top: 15,
                  }}
                >
                  {visible2 ? (
                    <EyeIcon width={20} height={20} />
                  ) : (
                    <EyeSlashIcon width={20} height={20} />
                  )}
                </TouchableOpacity>
              </View>
            </View>
            {/* End Confirm Password */}
          </View>

          {/* Button */}
          <TouchableOpacity
            style={{
              marginHorizontal: 16,
              backgroundColor: "#18DCFF",
              borderRadius: 8,
              paddingVertical: 12,
              alignItems: "center",
              marginTop: 20,
            }}
          >
            <Text>Register</Text>
          </TouchableOpacity>
          {/* End Button */}
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

export default Register;
const style = StyleSheet.create({});
