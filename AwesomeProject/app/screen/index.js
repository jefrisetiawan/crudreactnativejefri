import Profile from "./Profile";
import Main from "./Main";
import Home from "./Home";
import Task from "./Task";
import Performance from "./Perform";
import Add from "./Add";

import Login from "./Login";
import Register from "./Register";

export { Profile, Main, Home, Task, Performance, Add, Login, Register };
