import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import React, { useEffect, useState } from "react";
import images, { EyeIcon, EyeSlashIcon } from "../../assets/images";
import { Fonts, HEIGHT, WIDTH } from "../../assets/styles";
import Text from "../../component/Text";
import { TextInput } from "react-native-gesture-handler";
import Satellite from "../../services/satellite";

import { store } from "../../store/storage";
import { saveProfile } from "../../store/action/actionProfile";
const { dispath } = store;

const Login = ({ navigation, route }) => {
  const [isEnable, setIsEnable] = useState(true);

  //   useState => asyncronous (butuh waktu)
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [visible, setVisible] = useState(true);
  const [errorPass, setErrorPass] = useState("");
  const [errorMail, setErrorMail] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  // Custom Hook
  // Ketika state ada perubahan => load ulang mencoba membaca lagi State
  useEffect(() => {
    validation();
  }, [password, email]);

  const validation = () => {
    // console.log("INI VALIDASI");
    let vMail =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email && password) {
      if (vMail.test(email)) {
        setErrorMail("");
      } else {
        setErrorMail("Email not valid");
      }
      if (password.length > 0 && password.length <= 8) {
        setErrorPass("Password not valid");
      } else {
        setErrorPass("");
      }
      setIsEnable(false);
    } else {
      if (password) {
        setErrorPass("");
      } else {
        setErrorPass("Password must be filled in");
      }
      if (email) {
        setErrorMail("");
      } else {
        setErrorMail("Email must be filled in");
      }
      setIsEnable(true);
    }
  };

  //   loginAuth = async () => {
  //     const body = {
  //       email: email,
  //       password: password,
  //     };
  //     setIsLoading(true);
  //     setIsEnable(true);
  //     try {
  //       // dieksekusi ketika berhasil
  //       const login = await Satellite.post("auth/login", body);
  //       console.log("LOGIN DATA", JSON.stringify(login.data, null, 2));
  //     } catch (error) {
  //       // dieksekusi ketika error
  //       console.log(
  //         "LOGIN DATA Error",
  //         JSON.stringify(error.response.data, null, 2)
  //       );

  //     //   setErrorMail("Email not valid");
  //     //   setErrorPass("Password not valid");
  //     } finally {
  //       // dieksekusi ketika response selesai error maupun berhasil
  //       setIsLoading(false);
  //       setIsEnable(false);
  //     }
  //   };

  const loginAuth = () => {
    const body = {
      email: email,
      password: password,
    };
    setIsLoading(true);
    setIsEnable(true);
    Satellite.post("auth/login", body)
      .then((res) => {
        console.log("LOGIN DATA", JSON.stringify(res.data, null, 2));
        // dispath(saveProfile(res.data));
        navigation.navigate("Main");
      })
      .catch((error) => {
        console.log(
          "LOGIN DATA Error",
          JSON.stringify(error.response.data, null, 2)
        );
        setErrorMail("Email not valid");
        setErrorPass("Password not valid");
      })
      .finally(() => {
        setIsLoading(false);
        setIsEnable(false);
      });
  };

  return (
    <ImageBackground
      source={images.BGSCREEN}
      resizeMode="cover"
      style={{ width: WIDTH, height: HEIGHT }}
    >
      <View style={{ marginTop: 90, marginHorizontal: 16 }}>
        {/* Email */}
        <View>
          <Text fontSize={16} color={"#FFFF"}>
            Email
          </Text>
          <View
            style={{
              marginTop: 8,
              borderColor: errorMail ? "#EA8685" : "#132040",
              borderRadius: 8,
              borderWidth: 1,
              padding: 12,
              backgroundColor: "#273C75",
            }}
          >
            <TextInput
              value={email}
              keyboardType={"email-address"}
              inputMode={"email"}
              style={{
                color: "#FFF",
                fontSize: 16,
                fontFamily: Fonts.Nunito.Regular,
              }}
              placeholder="Enter Your Email"
              placeholderTextColor={"#D3D3D3"}
              onChangeText={(value) => {
                console.log("VALUE EMAIL", value);
                setEmail(value);
                validation();
              }}
            />
          </View>
          <View style={{ position: "absolute", bottom: -25, right: 5 }}>
            <Text color="#EA8685">{errorMail}</Text>
          </View>
        </View>

        {/* Password */}
        <View style={{ marginTop: 27 }}>
          <Text fontSize={16} color={"#FFFF"}>
            Password
          </Text>
          <View
            style={{
              marginTop: 8,
              borderColor: errorPass ? "#EA8685" : "#132040",
              borderRadius: 8,
              borderWidth: 1,
              padding: 12,
              backgroundColor: "#273C75",
            }}
          >
            <TextInput
              secureTextEntry={visible}
              value={password}
              style={{
                color: "#FFF",
                fontSize: 16,
                fontFamily: Fonts.Nunito.Regular,
              }}
              placeholder="Enter Your Password"
              placeholderTextColor={"#D3D3D3"}
              onChangeText={(value) => {
                console.log("VALUE PASSWORD", value);
                setPassword(value);
                validation();
              }}
            />

            <View style={{ position: "absolute", bottom: -25, right: 5 }}>
              <Text color="#EA8685">{errorPass}</Text>
            </View>

            {/* Eye */}
            <TouchableOpacity
              onPress={() => setVisible(!visible)}
              style={{
                position: "absolute",
                right: 12,
                top: 15,
              }}
            >
              {visible ? (
                <EyeIcon width={20} height={20} />
              ) : (
                <EyeSlashIcon width={20} height={20} />
              )}
            </TouchableOpacity>
          </View>

          {/* Forgot Password */}
          <Text color="#F6E58D" style={{ marginTop: 5, marginLeft: 5 }}>
            Forgot Password?
          </Text>
        </View>
      </View>
      {/* Button Login */}
      <TouchableOpacity
        disabled={isEnable}
        onPress={loginAuth}
        style={{
          opacity: isEnable ? 0.5 : 1,
          marginHorizontal: 16,
          backgroundColor: "#18DCFF",
          borderRadius: 8,
          paddingVertical: 12,
          alignItems: "center",
          marginTop: 20,
        }}
      >
        {isLoading ? (
          <ActivityIndicator size="small" color="#160520" />
        ) : (
          <Text>Login</Text>
        )}
      </TouchableOpacity>

      <View style={{ alignItems: "center", marginTop: 20 }}>
        <Text color="#FFF">
          Dont Have an Account?{" "}
          <Text onPress={() => navigation.navigate("Register")} color="#F6E58D">
            Sign Up
          </Text>
        </Text>
      </View>
    </ImageBackground>
  );
};

export default Login;

const style = StyleSheet.create({});
