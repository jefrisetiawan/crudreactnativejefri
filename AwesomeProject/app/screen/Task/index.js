import {
  ActivityIndicator,
  Button,
  FlatList,
  ScrollView,
  View,
} from "react-native";
import Text from "../../component/Text";
import React, { useEffect, useState } from "react";
import { HEIGHT, WIDTH } from "../../assets/styles";
import {
  app,
  db,
  getFirestore,
  collection,
  addDoc,
  getDocs,
} from "../../../firebaseConfig.js";
import { doc, onSnapshot } from "firebase/firestore";
import TaskItem from "../../component/TaskItem";
import { useFocusEffect } from "@react-navigation/native";

const Task = ({ navigation, route }) => {
  // const [tasksList, setTasksList] = useState([]);
  const [tasksList, setTasksList] = useState([]);

  const getTasksList = async () => {
    setTasksList([]);
    const myTask = [];
    const querySnapshot = await getDocs(collection(db, "tasks"));
    querySnapshot.forEach((doc) => {
      console.log(doc.id, doc.data());
      myTask.push({
        key: doc.id,
        title: doc.data().title,
        date: doc.data().date,
        activities: doc.data().activities,
        status: doc.data().status,
        start: doc.data().start,
        end: doc.data().end,
      });
    });
    setTasksList(myTask);
  };

  const unsubscribe = navigation.addListener("focus", () => {
    getTasksList();
  });

  useEffect(() => {
    getTasksList();
  }, []);

  return (
    <View>
      <View style={{ backgroundColor: "#FFF", width: WIDTH, height: HEIGHT }}>
        <View style={{ marginTop: 38, marginHorizontal: 16 }}>
          <Text fontSize={30} bold color="#160520">
            Time Sheet
          </Text>

          <View>
            {/* Flastlist */}
            <FlatList
              data={tasksList}
              renderItem={({ item }) => (
                <TaskItem
                  title={item.title}
                  date={item.date}
                  activities={item.activities}
                  status={item.status}
                  start={item.start}
                  end={item.end}
                />
              )}
              keyExtractor={(item, index) => {
                return item.id;
              }}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default Task;
