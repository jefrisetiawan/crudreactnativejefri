import {
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import React from "react";

import Text from "../../component/Text/index";

import { HEIGHT, Shadow, WIDTH, horizontalScale } from "../../assets/styles";
import images from "../../assets/images";

const Profile = ({ navigation, route }) => {
  return (
    <ScrollView>
      <View>
        <ImageBackground
          source={images.BACKGROUND}
          resizeMode="cover"
          style={{
            width: WIDTH,
            height: 495,
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{
              marginTop: 65,
              borderWidth: 2,
              borderRadius: 90,
              borderColor: "#FBD2A5",
            }}
          >
            <Image
              source={images.PERSON1}
              style={{
                width: 120,
                height: 120,
              }}
            />
          </TouchableOpacity>

          <View style={{ marginTop: 16, alignItems: "center" }}>
            <Text bold fontSize={20}>
              Jefri Setiawan
            </Text>
            <Text color="#909090" bold>
              React Native Developer
            </Text>
          </View>
        </ImageBackground>

        <View style={{ marginHorizontal: 16, marginTop: -195 }}>
          {/* Section 1 */}
          <View
            style={[
              {
                padding: 16,
                borderRadius: 12,
                backgroundColor: "#FFFF",
              },
              Shadow,
            ]}
          >
            <View style={{ flexDirection: "row" }}>
              <Text bold style={{ flex: 1 }}>
                ID
              </Text>
              <Text
                bold
                color="#A7A7A7"
                style={{ flex: 1, textAlign: "right" }}
              >
                A20134
              </Text>
            </View>

            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#D3D3D3",
                marginVertical: 13,
              }}
            />

            <View style={{ flexDirection: "row" }}>
              <Text bold style={{ flex: 1 }}>
                Email
              </Text>
              <Text
                bold
                color="#A7A7A7"
                style={{ flex: 1, textAlign: "right" }}
              >
                jef.stwn@gmail.com
              </Text>
            </View>

            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#D3D3D3",
                marginVertical: 13,
              }}
            />

            <View style={{ flexDirection: "row" }}>
              <Text bold style={{ flex: 1 }}>
                Date of Birth
              </Text>
              <Text
                bold
                color="#A7A7A7"
                style={{ flex: 1, textAlign: "right" }}
              >
                17 May 2023
              </Text>
            </View>

            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#D3D3D3",
                marginVertical: 13,
              }}
            />

            <View style={{ flexDirection: "row" }}>
              <Text bold style={{ flex: 1 }}>
                Gender
              </Text>
              <Text
                bold
                color="#A7A7A7"
                style={{ flex: 1, textAlign: "right" }}
              >
                Male
              </Text>
            </View>
          </View>

          {/* Section 2 */}
          <View
            style={[
              {
                padding: 16,
                borderRadius: 12,
                backgroundColor: "#FFFF",
                marginTop: 20,
              },
              Shadow,
            ]}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ flex: 1 }}>
                <Text bold>Team</Text>
                <Text bold color="#A7A7A7">
                  React Native
                </Text>
              </View>

              <Image
                source={images.PERSON2}
                style={{
                  width: 35,
                  height: 35,
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderRadius: 90,
                }}
              ></Image>
              <Image
                source={images.PERSON3}
                style={{
                  width: 35,
                  height: 35,
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderRadius: 90,
                  marginLeft: -15,
                }}
              ></Image>
              <Image
                source={images.PERSON4}
                style={{
                  width: 35,
                  height: 35,
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderRadius: 90,
                  marginLeft: -15,
                }}
              ></Image>
              <View
                style={{
                  borderWidth: 1,
                  width: 35,
                  height: 35,
                  backgroundColor: "#C16262",
                  borderRadius: 90,
                  borderColor: "#fff",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  marginLeft: -15,
                }}
              >
                <Text color="white">+6</Text>
              </View>
            </View>
          </View>

          {/* Section 3 */}
          <View
            style={[
              {
                padding: 16,
                borderRadius: 12,
                backgroundColor: "#FFFF",
                marginTop: 20,
              },
              Shadow,
            ]}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View
                style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
              >
                <Image
                  source={images.iGUARD}
                  style={{ width: 24, height: 24 }}
                ></Image>
                <Text bold style={{ marginLeft: 11 }}>
                  Privacy and Security
                </Text>
              </View>
              <Image
                source={images.rARROW}
                style={{ width: 5, height: 11 }}
              ></Image>
            </View>

            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#D3D3D3",
                marginVertical: 13,
              }}
            />

            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View
                style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
              >
                <Image
                  source={images.iHELP}
                  style={{ width: 24, height: 24 }}
                ></Image>
                <Text bold style={{ marginLeft: 11 }}>
                  Help
                </Text>
              </View>
              <Image
                source={images.rARROW}
                style={{ width: 5, height: 11 }}
              ></Image>
            </View>

            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#D3D3D3",
                marginVertical: 13,
              }}
            />
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View
                style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
              >
                <Image
                  source={images.iABOUT}
                  style={{ width: 24, height: 24 }}
                ></Image>
                <Text bold style={{ marginLeft: 11 }}>
                  About Us
                </Text>
              </View>
              <Image
                source={images.rARROW}
                style={{ width: 5, height: 11 }}
              ></Image>
            </View>

            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#D3D3D3",
                marginVertical: 13,
              }}
            />
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View
                style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
              >
                <Image
                  source={images.iLOGOUT}
                  style={{ width: 24, height: 24 }}
                ></Image>
                <Text bold style={{ marginLeft: 11 }}>
                  Logout
                </Text>
              </View>
              <Image
                source={images.rARROW}
                style={{ width: 5, height: 11 }}
              ></Image>
            </View>
          </View>

          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <Text bold style={{ marginTop: 20, marginBottom: 34 }}>
              v0.0.1
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Profile;

const styles = StyleSheet.create({});
