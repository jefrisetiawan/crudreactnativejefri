// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore, collection, addDoc, getDocs } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD-d3kS_Lg7jMQ4EVEQCfc_W4i7trz3ZY0",
  authDomain: "firstcrud-d9236.firebaseapp.com",
  projectId: "firstcrud-d9236",
  storageBucket: "firstcrud-d9236.appspot.com",
  messagingSenderId: "444276412521",
  appId: "1:444276412521:web:3b78a3186053f33be592f8",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { app, db, getFirestore, collection, addDoc, getDocs };
